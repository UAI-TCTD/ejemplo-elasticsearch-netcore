﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ElasticSearchSample.WEB.Models;
using ElasticSearchSample.Core;

namespace ElasticSearchSample.WEB.Controllers
{
    public class HomeController : Controller
    {
        ElasticSample _es;
        public HomeController()
        {
            _es = new ElasticSample();
        }


        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public IActionResult buscar(SearchModel model)
        {
          var res=  _es.Buscar(model.Texto);



            ViewData["result"] = res;

            return View("index");
        }

        
        public IActionResult CrearProducto()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CrearProducto([FromForm] Producto p)
        {
            if (ModelState.IsValid)
            {
                _es.CrearProduto(p);
            }

            return View();
        }

        public IActionResult CrearIndice()
        {

            _es.CrearIndice();
            return View("index");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
