﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ElasticSearchSample.Core
{
    public class ElasticSample
    {
        string host = "http://192.168.0.28:9200";
        ElasticClient _client;
        public ElasticSample()
        {

            var settings = new ConnectionSettings(new Uri($"{host}"))
             .DefaultIndex("idx_productos");
            _client = new ElasticClient(settings);
        }


        public void CrearProduto(Producto p)
        {
            var indexResponse = _client.IndexDocument(p);
        }


        public void SimularProductos()
        {
            var p = new Producto()
            {
                Marca = "moulinex",
                Modelo = "HOR1011",
                Nombre = "horno microondas de 50L",
                Precio = 1044,
                Descripcion="Horno microondas de 50L con anafé y grill automático"
            };

            CrearProduto(p);

            p = new Producto()
            {
                Marca = "moulinex",
                Modelo = "HOR1010",
                Nombre = "horno microondas de 30L",
                Precio = 1044,
                Descripcion = "Horno microondas de 30L con grill semiautomáticpo"
            };
            CrearProduto(p);

                 p = new Producto()
                 {
                     Marca = "black and decker",
                     Modelo = "Hor1000",
                     Nombre = "horno electrico de 40L",
                     Precio = 1344,
                     Descripcion = "Horno eléctrico de 40L con grill y asador"
                 };
            CrearProduto(p);

            p = new Producto()
            {
                Marca = "black and decker",
                Modelo = "Bat133",
                Nombre = "batidora",
                Precio = 3344,
                Descripcion = "Licuadora especial autática con control de velocidad"
            };
            CrearProduto(p);


            p = new Producto()
            {
                Marca = "black and decker",
                Modelo = "Bord1344",
                Nombre = "Bordeadora",
                Precio = 5344,
                Descripcion = "Bordeadora con dispenser automático de tanza"
            };
            CrearProduto(p);
        }




        public void CrearIndice()
        {
            //
            //https://www.elastic.co/guide/en/elasticsearch/client/net-api/current/fluent-mapping.html
            var createIndexResponse = _client.Indices.Create("idx_productos", c => c
                
            .Settings(s=>s
                .Analysis(a=>a
                    .Normalizers(n=>n
                            .Custom("useLowerCase",n1=>n1.Filters("lowercase"))
                      )
                    .TokenFilters(tf=>tf
                        .Lowercase("lowercase")
                        .NGram("autocomplete",e1=>e1.MinGram(3).MaxGram(4))
                        .AsciiFolding("my_ascii_folding",a1=>a1.PreserveOriginal(false))
                        .Stop("spanish_stop",s1=>s1.StopWords(new StopWords("_spanish_")))
                        .Stemmer("spanish_stemmer",s2=>s2.Language("spanish"))
                        

                    )
                    .Analyzers(a2=>a2
                       .Custom("my_keyword",c1=>c1.Filters("lowercase", "my_ascii_folding", "spanish_stemmer").Tokenizer("standard"))
                       .Custom("autocomplete",c2=>c2.Filters("lowercase", "my_ascii_folding", "autocomplete").Tokenizer("standard"))
                    
                    )
                )
            )            
            .Map<Producto>(m => m
                    .Properties(ps => ps
                        .Text(s => s
                            .Name(n => n.Nombre)
                            .SearchAnalyzer("autocomplete")
                            .Analyzer("autocomplete")
                        )
                         .Text(s => s
                                .Name(e => e.Descripcion)
                                .SearchAnalyzer("autocomplete")
                                 .Analyzer("autocomplete")
                                )
                         .Keyword(s => s
                                    .Name(e => e.Marca)
                                    
                                    
                                   )
                         .Keyword(s => s
                                    .Name(e => e.Modelo)
                                )
                         .Number(n => n
                                    .Name(e => e.Precio)
                                    .Type(NumberType.Integer)
                                )
                        )
                    )
                );

            SimularProductos();
           
        }

        public ProductoResult Buscar(string texto)
        {
            var searchResponse = _client.Search<Producto>(s => s
                    
                        .Aggregations(ag=>ag
                            .Stats("stats",sa=>sa  
                                .Field(fs=>fs.Precio)
                                )
                            .Terms("marca",at=>at   
                                .Field(fm=>fm.Marca)
                                )
                        )
                        .Query(q => q
                            
                             .MultiMatch(m => m
                                .Type(TextQueryType.MostFields)
                                .Fields(f=>f
                                    .Field("descripcion")
                                    .Field("nombre")
                                    .Field("marca")
                                 )
                                .Query(texto)
                             )
                        )
                    );

         
            var lista = searchResponse.Hits.Select(g => g.Source).ToList();

            StatsAggregate aggStats =(StatsAggregate) searchResponse.Aggregations["stats"];

            BucketAggregate aggBucket = (BucketAggregate)searchResponse.Aggregations["marca"];

            var d = new Dictionary<string, long?>();

            foreach (var item in aggBucket.Items)
            {

                var i = ((KeyedBucket<object>)item);
                d.Add(i.Key.ToString(), i.DocCount);

                
            }

            return new ProductoResult()
            {
                Productos = lista,
                Max = aggStats.Max,
                Min= aggStats.Min,
                Avg= aggStats.Average,
                Count= aggStats.Count,
                Marcas=d
            };
        }

    }
}
