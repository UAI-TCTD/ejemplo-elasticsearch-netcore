﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticSearchSample.Core
{
    public class Producto
    {
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public int Precio { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }

    }

   
}
