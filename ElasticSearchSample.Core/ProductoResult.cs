﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticSearchSample.Core
{
    public class ProductoResult
    {
        public ProductoResult()
        {
            Productos = new List<Producto>();
        }
       public List<Producto> Productos { get; set; }

        public double? Avg { get; set; }
        public double? Max { get; set; }
        public double? Min { get; set; }
        public long Count { get; set; }
        public Dictionary<string, long?> Marcas { get; internal set; }
    }
}
